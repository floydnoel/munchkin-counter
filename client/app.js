// counter starts at 0
Session.setDefault('players', [
	'player 1',
	'player 2',
	'player 3',
	'player 4'
]);

Session.get('players').forEach(function (e, i, a) {
	Session.setDefault(e, 1);
});

Template.main.helpers({
  players: function () {
    return Session.get('players');
  },
  playerScore: function (id) {
    return Session.get(id);
  }
});

Template.main.events({
  'click .decrement': function (e) {
    e.preventDefault();
    decrementPlayer(e.currentTarget.id);
  },
  'click .increment': function (e) {
    e.preventDefault();
    incrementPlayer(e.currentTarget.id);
  },
  'click .close': function (e) {
    $(e.currentTarget.parentElement).transition('fade');
    // setPlayers(['foo', 'bar', 'baz']);
  },
  'click #configurePlayersButton': function (e) {
    e.preventDefault();
    $('#playerConfigurationPanel').toggle();
  },
  'click #savePlayerConfiguration': function (e) {
    e.preventDefault();
    setPlayers([
      $('#playerName01').val(),
      $('#playerName02').val(),
      $('#playerName03').val(),
      $('#playerName04').val()
    ]);
  }
});

function incrementPlayer(playerId) {
  oldScore = Session.get(playerId);
  Session.set(playerId, oldScore + 1);
}

function decrementPlayer(playerId) {
  oldScore = Session.get(playerId);
  Session.set(playerId, oldScore - 1);
}

// takes an array of new player names
function setPlayers(newPlayers) {
  Session.set('players', newPlayers);
  Session.get('players').forEach(function (e, i, a) {
    Session.set(e, 1);
  });
}
