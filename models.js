var Schemas = {};

Schemas.Player = new SimpleSchema({

  name: {
    type: String,
    label: 'Player name'
  },

  counters: {
    type: [Number],
    label: 'Counters'
  }

});

Schemas.Game = new SimpleSchema({

  player: {
    type: [Schemas.player],
    label: 'Player N'
  },
});

Games = new Mongo.Collection('games');

Games.attachSchema(Schemas.Game);

// Players = new Mongo.Collection("players");

// Players.attachSchema(Schemas.game);

